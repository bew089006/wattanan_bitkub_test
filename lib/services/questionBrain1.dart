import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class QuestionBrain1 {
  Widget getAnswer1(int n) {
    List<Row> resultUp = new List<Row>();
    List<Row> resultDown = new List<Row>();

    for (int i = n; i >= 0; i--) {
      if (i != 0) {
        Widget widgetUp = Row(
          children: [
            for (int j = 0; j < n - i; j++)
              SizedBox(
                  width: 10, child: Text("O", textAlign: TextAlign.center)),
            for (int j = 0; j < i - 1; j++)
              Row(
                children: [
                  SizedBox(
                      width: 10, child: Text("X", textAlign: TextAlign.center)),
                  SizedBox(
                      width: 10, child: Text("O", textAlign: TextAlign.center)),
                ],
              ),
            SizedBox(width: 10, child: Text("X", textAlign: TextAlign.center)),
            for (int j = 0; j < n - i; j++)
              SizedBox(
                  width: 10, child: Text("O", textAlign: TextAlign.center)),
          ],
        );
        resultUp.add(widgetUp);
      }

      if (i != n && i != n - 1) {
        Widget widgetDown = Row(
          children: [
            for (int j = 0; j < i; j++)
              SizedBox(
                  width: 10, child: Text("O", textAlign: TextAlign.center)),
            for (int j = 0; j < (n - i) - 1; j++)
              Row(
                children: [
                  SizedBox(
                      width: 10, child: Text("X", textAlign: TextAlign.center)),
                  SizedBox(
                      width: 10, child: Text("O", textAlign: TextAlign.center)),
                ],
              ),
            SizedBox(width: 10, child: Text("X", textAlign: TextAlign.center)),
            for (int j = 0; j < i; j++)
              SizedBox(
                  width: 10, child: Text("O", textAlign: TextAlign.center)),
          ],
        );
        resultDown.add(widgetDown);
      }
    }

    return Column(children: resultUp + resultDown);
  }

  Widget getAnswer2(int n) {
    double center = n / 2;
    List<Widget> resultUp = new List<Widget>();
    List<Widget> resultDown = new List<Widget>();

    Widget _genStrAnswer(i) {
      int voidLength = (n - (2 * i)).abs();

      return Row(
        children: [
          for (int j = 0; j < ((n - voidLength) / 2).round(); j++)
            SizedBox(
              width: 10,
              child: Text("X", textAlign: TextAlign.center),
            ),
          for (int j = 0; j < (n - (2 * i)).abs(); j++)
            SizedBox(
              width: 10,
              child: Text(" ", textAlign: TextAlign.center),
            ),
          for (int j = 0; j < ((n - voidLength) / 2).round(); j++)
            SizedBox(
              width: 10,
              child: Text("X", textAlign: TextAlign.center),
            ),
        ],
      );
    }

    for (int i = 1; i < n; i++) {
      if (i < center) {
        resultUp.add(_genStrAnswer(i));
      } else if (i > center) {
        resultDown.add(_genStrAnswer(i));
      }
    }

    if (n % 2 != 0) {
      Widget rowCenter = Row(
        children: [
          for (int j = 0; j < n; j++)
            SizedBox(
              width: 10,
              child: Text("X", textAlign: TextAlign.center),
            ),
        ],
      );

      resultUp.add(rowCenter);
    } else {
      Widget rowCenter = Row(
        children: [
          for (int j = 0; j < n; j++)
            SizedBox(
              width: 10,
              child: Text("X", textAlign: TextAlign.center),
            ),
        ],
      );
      resultUp.add(rowCenter);
      resultUp.add(rowCenter);
    }

    return Column(children: resultUp + resultDown);
  }

  Widget getAnswer3(int n) {
    List<Row> resultRows = new List<Row>();
    double center = n / 2;
    int centerUp = center.round();

    for (int i = 1; i <= n; i++) {
      if (n / 2 > 1) {
        if (i == 2) {
          resultRows.add(
            Row(
              children: [
                for (int j = 0; j < (n - 1); j++)
                  SizedBox(
                    width: 10,
                    child: Text(
                      "Y",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.red,
                      ),
                    ),
                  ),
                SizedBox(
                  width: 10,
                  child: Text("X", textAlign: TextAlign.center),
                ),
              ],
            ),
          );
        } else if (i == centerUp) {
          if (n >= 7) {
            resultRows.add(Row(
              children: [
                SizedBox(
                    width: 10, child: Text("X", textAlign: TextAlign.center)),
                for (int j = 0; j < (n - 4); j++)
                  SizedBox(
                      width: 10,
                      child: Text(
                        "Y",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.red,
                        ),
                      )),
                SizedBox(
                    width: 10, child: Text("X", textAlign: TextAlign.center)),
                SizedBox(
                    width: 10,
                    child: Text(
                      "Y",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.red,
                      ),
                    )),
                SizedBox(
                    width: 10, child: Text("X", textAlign: TextAlign.center)),
              ],
            ));
          } else {
            resultRows.add(Row(
              children: [
                for (int j = 0; j < (n - 2); j++)
                  SizedBox(
                      width: 10, child: Text("X", textAlign: TextAlign.center)),
                SizedBox(
                    width: 10,
                    child: Text(
                      "Y",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.red,
                      ),
                    )),
                SizedBox(
                    width: 10, child: Text("X", textAlign: TextAlign.center)),
              ],
            ));
          }
        } else if (i == n - 1) {
          if (n > 4) {
            resultRows.add(Row(
              children: [
                SizedBox(
                    width: 10, child: Text("X", textAlign: TextAlign.center)),
                for (int j = 0; j < (n - 2); j++)
                  SizedBox(
                      width: 10,
                      child: Text(
                        "Y",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.red,
                        ),
                      )),
                SizedBox(
                    width: 10, child: Text("X", textAlign: TextAlign.center)),
              ],
            ));
          } else {
            resultRows.add(Row(
              children: [
                for (int j = 0; j < (n - 2); j++)
                  SizedBox(
                      width: 10, child: Text("X", textAlign: TextAlign.center)),
                SizedBox(
                    width: 10,
                    child: Text(
                      "Y",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: Colors.red,
                      ),
                    )),
                SizedBox(
                    width: 10, child: Text("X", textAlign: TextAlign.center)),
              ],
            ));
          }
        } else if (i > centerUp && i < n - 1) {
          resultRows.add(Row(
            children: [
              SizedBox(
                  width: 10, child: Text("X", textAlign: TextAlign.center)),
              SizedBox(
                  width: 10,
                  child: Text(
                    "Y",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.red,
                    ),
                  )),
              for (int j = 0; j < (n - 4); j++)
                SizedBox(
                    width: 10, child: Text("X", textAlign: TextAlign.center)),
              SizedBox(
                  width: 10,
                  child: Text(
                    "Y",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.red,
                    ),
                  )),
              SizedBox(
                  width: 10, child: Text("X", textAlign: TextAlign.center)),
            ],
          ));
        } else if (i > 2 && i < centerUp) {
          resultRows.add(Row(
            children: [
              for (int j = 0; j < (n - 2); j++)
                SizedBox(
                    width: 10, child: Text("X", textAlign: TextAlign.center)),
              SizedBox(
                  width: 10,
                  child: Text(
                    "Y",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.red,
                    ),
                  )),
              SizedBox(
                  width: 10, child: Text("X", textAlign: TextAlign.center)),
            ],
          ));
        } else {
          resultRows.add(Row(
            children: [
              for (int j = 0; j < n; j++)
                SizedBox(
                    width: 10, child: Text("X", textAlign: TextAlign.center)),
            ],
          ));
        }
      } else {
        resultRows.add(Row(
          children: [
            for (int j = 0; j < n; j++)
              SizedBox(
                  width: 10, child: Text("X", textAlign: TextAlign.center)),
          ],
        ));
      }
    }

    return Column(children: resultRows);
  }
}
