import 'package:flutter/material.dart';
import 'constants.dart';
import 'package:http/http.dart' as Http;
import 'dart:convert';

class QuestionThree extends StatefulWidget {
  @override
  _QuestionThreeState createState() => _QuestionThreeState();
}

class _QuestionThreeState extends State<QuestionThree> {
  String _url = "https://jsonplaceholder.typicode.com/users";

  @override
  Widget build(BuildContext context) {
    return MyScaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: ListView(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Example 3."),
                ],
              ),
              SizedBox(
                height: 50,
              ),
              Container(
                child: FutureBuilder(
                  future: Http.get(_url),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done) {
                      List responseData = json.decode(snapshot.data.body);

                      return Column(
                        children: [
                          if (responseData != null)
                            for (var data in responseData)
                              SizedBox(
                                width: double.infinity,
                                height: 250,
                                child: Expanded(
                                  child: Card(
                                    child: Padding(
                                      padding: const EdgeInsets.all(5.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text("id: ${data["id"]}"),
                                          Text("name: ${data["name"]}"),
                                          Text("username: ${data["username"]}"),
                                          Text("email: ${data["email"]}"),
                                          Text("phone: ${data["phone"]}"),
                                          Text("address: ${data["address"]["street"]} ${data["address"]["suite"]} \n" +
                                              "${data["address"]["city"]} ${data["address"]["zipcode"]}"),
                                          Text("website: ${data["website"]}"),
                                          Text(
                                              "company name: ${data["company"]["name"]}"),
                                          Text(
                                              "company catchPhrase: ${data["company"]["catchPhrase"]}"),
                                          Text(
                                              "company bs: ${data["company"]["bs"]}"),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                        ],
                      );
                    } else {
                      return CircularProgressIndicator();
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
